<b>Zadatak:</b>

• Napraviti novi MVC web projekat. Projekat treba da simulira sistem biblioteke.<br/> Na početnoj stranici napraviti formu za dodavanje novih knjiga <br/>
Mogući žanrovi za knjige su:<br/>
• Science<br/>
• Comedy<br/>
• Horror<br/>

Automatski se prelazi na Bookstore/List tj. istu akciju koja se pokreće klikom na link „Show all books“ <br/>
Na dugme Add se dodaje nova knjiga. <br/> Postoji  mogućnost izlistavanja svih i prikaz obrisanih knjiga.<br/>
Model podataka <br/>
Klasa Bookstore.cs mora da sadrži:<br/>
• Id<br/>
• Name<br/>
• Listu Book elemenata<br/>
 Klasa Book.cs sadrži:<br/>
• Id <br/> 
• Name <br/>
• Price <br/>
• Genre <br/>
• Deleted <br/>

• Svaki red u tabeli na kraju ima link ka delete akciji koja briše unos (knjigu) u datom redu postavivši joj property Deleted na true, <br/> i
ponovo se vrši listing svih dodatih knjiga (ponovo se ispisuju knjige). <br/>
• Klikom na „link for homepage“, korisnik se vraća na početnu stranicu gde je prikazana forma za dodavanje knjiga <br/>
• Na ovom listingu se prikazuju samo one knjige koje nisu obrisane (deleted = false) <br/>
• Klikom na „Show deleted books“, treba omogućiti listing svih obrisanih knjiga. <br/>
• Omogućiti sortiranje svih ispisanih knjiga po nazivu ili po ceni, rastuće ili opadajuće (znači sve 4 kombinacije). <br/>
• Bilo bi poželjno omogućiti putem dropdown menija (html select element)<br/>