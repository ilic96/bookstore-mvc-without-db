﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booksotre_static_MVC.Models
{
    public class Bookstore
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Book> Books { get; set; }

        public Bookstore(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
            this.Books = new List<Book>();
        }
    }
}