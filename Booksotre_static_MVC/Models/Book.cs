﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booksotre_static_MVC.Models
{
    public enum Genre
    {
        Science,
        Comedy,
        Horror
    }

    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Genre genre { get; set; }
        public bool Deleted { get; set; }

        public Book(int Id, string Name, decimal Price, Genre genre, bool Deleted)
        {
            this.Id = Id;
            this.Name = Name;
            this.Price = Price;
            this.genre = genre;
            this.Deleted = Deleted;
        }
    }
}