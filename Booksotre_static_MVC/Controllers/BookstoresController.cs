﻿using Booksotre_static_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booksotre_static_MVC.Controllers
{
    public class BookstoresController : Controller
    {
        private static Bookstore bookstore;

        static BookstoresController()
        {
            bookstore = new Bookstore(1, "Vulkan");

            Book book1 = new Book(1, "Snesko", 1000m, Genre.Horror, false);
            Book book2 = new Book(2, "Sicilijanac", 2100m, Genre.Science, false);
            Book book3 = new Book(3, "Komsija", 899m, Genre.Comedy, false);

            bookstore.Books.Add(book1);
            bookstore.Books.Add(book2);
            bookstore.Books.Add(book3);
        }
        // GET: Bookstores
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddNew(string Name, decimal Price, Genre genre)
        {
            Book b = new Book(bookstore.Books.Count + 1, Name, Price, genre, false);
            bookstore.Books.Add(b);
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            ViewBag.books = bookstore.Books.Where(x => x.Deleted != true);
            return View();
        }

        public ActionResult Deleted()
        {
            ViewBag.deletedBooks = bookstore.Books.Where(x => x.Deleted == true);

            return View();
        }

        public ActionResult Delete(int id)
        {
            foreach(Book b in bookstore.Books)
            {
                if(b.Id == id)
                {
                    b.Deleted = true;
                }
            }
            return RedirectToAction("List");
        }

        public ActionResult Sort()
        {
            return View();
        }
        public ActionResult SortResult(int sortBy, int order)
        {
            if(sortBy == 1 && order == 1)
            {
                ViewBag.books = bookstore.Books.Where(x => x.Deleted == false).OrderBy(x => x.Price);
            }
            else if(sortBy == 1 && order == 2)
            {
                ViewBag.books = bookstore.Books.Where(x => x.Deleted == false).OrderByDescending(x => x.Price);
            }
            else if(sortBy == 2 && order == 1)
            {
                ViewBag.books = bookstore.Books.Where(x => x.Deleted == false).OrderBy(x => x.Name);
            }
            else
            {
                ViewBag.books = bookstore.Books.Where(x => x.Deleted == false).OrderByDescending(x => x.Name);
            }
            return View();
        }
    }
}